# PMDMA
## Description
PMDMA is a persistent memory file system with DMA.PMDMA imports [I/OAT driver](https://elixir.bootlin.com/linux/v5.1/source/drivers/dma/ioat) into persistent memory file system.PMDMA uses DMA devices to copy data between DRAM and PM when it reads and writes files. 

PMDMA provides strong read and write performance:

 - Better concurrency performance.
 - Higher bandwidth when reading and writing big data at once.
 - Higher read and write throughput.

Currently, PMDMA has been implemented in persistent memory file system  [NOVA](https://github.com/NVSL/NOVA) and [SplitFS](https://github.com/rohankadekodi/SplitFS).
## content

1. `nova-dma/`contains PMDMA source code based on NOVA.
2. `splitfs-dma/` contains PMDMA source code based on SplitFS.

You can learn more about NOVA-DMA and SplitFS-DMA by reading `READEME.md` in both directories.
## Building and Using
PMDMA based on different file systems is built and used differently.For details of NOVA-DMA construction and usage, click on link [NOVA-DMA Building and Using](https://gitlab.com/dingdly/pmdma/-/tree/master/nova-dma#building-and-using-nova-dma). For details of SplitFS-DMA construction and usage, click on link [SplitFS-DMA Building and Using](https://gitlab.com/dingdly/pmdma/-/tree/master/splitfs-dma#getting-started). 

## Current limitations

 - PMDMA only works on x86-64 kernels.
 - PMDMA only supports Intel processors with CBDMA

